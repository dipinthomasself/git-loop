#!/bin/bash

echo $1
echo $2

#=================
# Get GIT Message
#=================
git_message=`git log -1 | grep update`
echo "$git_message +jenkins"
echo $GIT_BRANCH


#=================
# Find action
#=================

ACTION="nahnah"
echo $git_message
if [[ {$git_message,,} =~ .*major.* ]] 
then
echo "found major, incrementing to major"
ACTION="major"
elif [[ {$git_message,,} =~ .*minor.* ]]
then
echo "Found minor"
ACTION="minor"
elif [[ {$git_message,,} =~ .*patch.* ]]
then 
echo "Found patch"
ACTION="patch"
else
echo "Nothing Found"
fi
echo "Action to be performed is $ACTION"

#=================
# Update action
#=================

npm --help
echo "The action is complete"

#=================
# Update bitbucket
#=================

touch file{1..2}


